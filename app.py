from tkinter import *
import cv2
import numpy as np
from PIL import ImageTk, Image
import time
from tracking_area import TrackingArea
from drosocam import Camera
from drosodetector import Detector
from asyncimagewriter import AsyncWriter

class Drosotracker:
    def __init__(self, master):
        self.master = master
        master.title("Drosotracker")

        # self.camera = cv2.VideoCapture(0)
        self.camera = Camera().init()
        # self.camera = cv2.VideoCapture("video.mp4")
        # self.static_image = cv2.imread("fly1.png")

        self.blobDetector = Detector().init()

        self.loop = True

        self.label = Label(master, text="This is our first GUI!")
        self.label.pack()

        self.tracking_area = None

        # self.cv2img = cv2.imread("test.jpg")
        self.cv2img = self.get_image()
        # b,g,r = cv2.split(self.cv2img)
        # self.img = cv2.merge((r,g,b))
        self.img = self.cv2img
        self.tkarrayimage = Image.fromarray(self.img)
        self.tkphotoimage = ImageTk.PhotoImage(image=self.tkarrayimage)

        self.videoStream = Label(master, image=self.tkphotoimage)
        self.videoStream.bind("<Button-1>", self.clicked)
        self.videoStream.pack()


        self.top = Frame(self.master)
        self.bottom = Frame(self.master)

        self.refresh_button = Button(self.top, text="Refresh", command=self.refresh)
        self.refresh_button.pack(in_=self.top, side="left")

        self.save_image_button = Button(self.top, text="Save Image", command=self.save_image)
        self.save_image_button.pack(in_=self.top, side="left")

        self.toggle_video_button = Button(self.top, text="Toggle Video", command=self.toggle_video)
        self.toggle_video_button.pack(in_=self.top, side="left")

        self.close_button = Button(self.top, text="Close", command=master.quit)
        self.close_button.pack(in_=self.top, side="left")

        self.top.pack(fill=BOTH, expand=True)

        self.width_entry_label = Label(self.bottom, text="Tracking Area Width").pack(side="left")
        self.width_entry = Entry(self.bottom)
        self.width_entry.insert(0, "30")
        self.width_entry.pack(side="left")

        self.length_entry = Entry(self.bottom)
        self.length_entry.insert(0, "30")
        self.length_entry.pack(side="right")
        self.length_entry_label = Label(self.bottom, text="Tracking Area Length").pack(side="right")

        self.bottom.pack(fill=BOTH, expand=True)

        self.video_loop()

    def video_loop(self):
        if (self.loop):
            self.refresh()
        self.master.after(30, self.video_loop)

    def detect_drosophila(self, image):
        return self.blobDetector.detect(image)

    def get_image(self):
        return self.camera.get_image()

    def draw_tracking_area(self):
        if (self.tracking_area):
            top_left_corner = (self.tracking_area.get_min_x(), self.tracking_area.get_min_y())
            bottom_right_corner = (self.tracking_area.get_max_x(), self.tracking_area.get_max_y())
            cv2.rectangle(self.processed_image, top_left_corner, bottom_right_corner, (255, 0, 0), 2)


    def refresh(self):
        self.cv2img = self.get_image()
        self.processed_image, keypoints = self.detect_drosophila(self.cv2img)
        self.draw_tracking_area()
        self.fly_in_tracking_area(keypoints)
        b,g,r = cv2.split(self.processed_image)
        self.img = cv2.merge((r,g,b))
        self.tkarrayimage = Image.fromarray(self.img)
        self.tkphotoimage = ImageTk.PhotoImage(image=self.tkarrayimage)
        self.videoStream.configure(image=self.tkphotoimage)
        self.videoStream.image = self.tkphotoimage

    def save_image(self):
        timestr = time.strftime("%Y%m%d-%H%M%S")
        # writer = AsyncWriter(timestr, self.cv2img)
        # writer.start()
        # writer.join()
        cv2.imwrite(timestr + ".jpg", self.cv2img)

    def toggle_video(self):
        self.loop = not self.loop

    def clicked(self, event):
        print(event.x, event.y)
        x, y = self.video_stream_to_tracker_coordinate_mapper(event.x, event.y)
        self.tracking_area = TrackingArea(x, y, int(self.width_entry.get()), int(self.length_entry.get()))

    def fly_in_tracking_area(self, keypoints):
        if (self.tracking_area):
            if (len(keypoints) > 0):

                fly_in_detection_area = False

                for keypoint in keypoints:
                    x = keypoint.pt[0]
                    y = keypoint.pt[1]

                    result = self.tracking_area.blob_within_tracking_area((x, y))
                    if (not fly_in_detection_area):
                        fly_in_detection_area = result
                if (fly_in_detection_area):
                    self.label.configure(text="FLY IN AREA")
                    self.label.configure(background="red")
                else:
                    self.label.configure(text="This is our first GUI!")
                    self.label.configure(background=self.master.cget("background"))

    def video_stream_to_tracker_coordinate_mapper(self, x, y):
        # It appears that there is a one-to-one correspondence
        # between the OpenCV video stream coordinates and the
        # tracker's coordinates
        return x, y


root = Tk()
my_gui = Drosotracker(root)
root.mainloop()