import unittest
from rectangle import Rectangle


class TestRectangle(unittest.TestCase):

    def test_construction_origin(self):
        rectangle = Rectangle(3, 2, 6, 8, "origin")
        self.assertEqual(rectangle.get_min_x(), 3)
        self.assertEqual(rectangle.get_min_y(), 2)
        self.assertEqual(rectangle.get_max_x(), 9)
        self.assertEqual(rectangle.get_max_y(), 10)
        origin = rectangle.get_origin()
        self.assertEqual(origin[0], 3)
        self.assertEqual(origin[1], 2)
        self.assertEqual(rectangle.get_width(), 8)
        self.assertEqual(rectangle.get_length(), 6)
        self.assertEqual(rectangle.get_type(), "origin")

    def test_construction_centroid(self):
        rectangle = Rectangle(6, 6, 6, 8, "centroid")
        self.assertEqual(rectangle.get_min_x(), 3)
        self.assertEqual(rectangle.get_min_y(), 2)
        self.assertEqual(rectangle.get_max_x(), 9)
        self.assertEqual(rectangle.get_max_y(), 10)
        origin = rectangle.get_origin()
        self.assertEqual(origin[0], 3)
        self.assertEqual(origin[1], 2)
        self.assertEqual(rectangle.get_width(), 8)
        self.assertEqual(rectangle.get_length(), 6)
        self.assertEqual(rectangle.get_type(), "centroid")