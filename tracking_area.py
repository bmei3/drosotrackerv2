"""
Uses a rectangle to specify a tracking area.

Generate a rectangle based on provided coordinates.
Assume that provided coordinates are the centroid of the tracking area

All coordinates assumed to be cartesian (x, y)
"""

from rectangle import Rectangle


class TrackingArea:
    def __init__(self, x, y, width, length):
        self.rectangle = Rectangle(x, y, width, length, "centroid")

    def get_min_x(self):
        return self.rectangle.get_min_x()
    
    def get_min_y(self):
        return self.rectangle.get_min_y()

    def get_max_x(self):
        return self.rectangle.get_max_x()

    def get_max_y(self):
        return self.rectangle.get_max_y()

    def blob_within_tracking_area(self, coordinates):
        min_x = self.rectangle.get_min_x()
        max_x = self.rectangle.get_max_x()
        min_y = self.rectangle.get_min_y()
        max_y = self.rectangle.get_max_y()

        return coordinates[0] >= min_x and \
               coordinates[0] <= max_x and \
               coordinates[1] >= min_y and \
               coordinates[1] <= max_y