# Drosotracker v2

## Setup
### Requirements
- Python 3.7.0

### Windows
- python -m venv .\venv
- .\venv\Scripts\activate
- pip install -r requirements.txt
- python -m unittest
- python app.py

### Unix
- python -m venv ./venv
- source ./venv/bin/activate
- pip install -r requirements.txt
- python -m unittest
- python app.py