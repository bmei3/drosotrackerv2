import threading
import cv2

class AsyncWriter(threading.Thread):
    def __init__(self, filename, filecontents):
        threading.Thread.__init__(self)
        self.filename = filename
        self.contents = filecontents
    
    def run(self):
        cv2.imwrite(self.filename + ".jpg", self.contents)