import cv2

class Camera:
    def __init__(self):
        self.camera = None
        self.debug = True

    def init(self):
        if (self.debug):
            self.camera = cv2.VideoCapture("video.mp4")
        else:
            self.camera = cv2.VideoCapture(0)
            self.camera.set(3, 1280)
            self.camera.set(4, 720)
        return self

    def get_image(self):
        ret, frame = self.camera.read()
        if (self.debug):
            frame = cv2.resize(frame, (1280, 720))
        return frame