import unittest
from tracking_area import TrackingArea

class TestTrackingArea(unittest.TestCase):

    def test_construction(self):
        tracking_area = TrackingArea(3, 2)

    def test_blob_in_tracking_area(self):
        tracking_area = TrackingArea(30, 30)
        coordinates_within_area = (20, 20)
        coordinates_without_area = (1, 1)
        self.assertTrue(tracking_area.blob_within_tracking_area(coordinates_within_area))
        self.assertFalse(tracking_area.blob_within_tracking_area(coordinates_without_area))