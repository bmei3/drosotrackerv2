"""
Given either the centroid of a rectangle or the origin,
create a rectangle with a specified length and width.

The origin of a rectangle is defined as its top left corner.

The centroid and origin should be specified in cartesian (x, y) coordinates

      origin         length
        +-------------------------------+
        |                               |
        |                               | width
        |                               |
        +-------------------------------+

"""

class Rectangle:
    def __init__(self, x, y, length, width, rectangle_type):
        self.length = length
        self.width = width
        if (rectangle_type == "centroid"):
            self.x = x - (length // 2)
            self.y = y - (width // 2)
            self.rectangle_type = "centroid"
        else:
            self.x = x
            self.y = y
            self.rectangle_type = "origin"
    
    def get_origin(self):
        return (self.x, self.y)

    def get_length(self):
        return self.length

    def get_min_x(self):
        return self.x

    def get_max_x(self):
        return self.x + self.length

    def get_min_y(self):
        return self.y

    def get_max_y(self):
        return self.y + self.width

    def get_width(self):
        return self.width

    def get_type(self):
        return self.rectangle_type