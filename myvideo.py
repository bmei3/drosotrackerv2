import cv2
import sys
import numpy as np

# cap = cv2.VideoCapture("video.mp4")
cap = cv2.VideoCapture(0)

cap.set(3, 1280)
cap.set(4, 720)

image = cv2.imread("fly1.png")

params = cv2.SimpleBlobDetector_Params()

params.filterByColor = False
# Change thresholds
params.minThreshold = 1
params.maxThreshold = 200
# Filter by Area.
params.filterByArea = True
# params.minArea = 20
params.minArea = 250
params.maxArea = 500
# Filter by Circularity
params.filterByCircularity = False
params.minCircularity = 0.1
params.maxCircularity = 0.5
# Filter by Convexity
params.filterByConvexity = False
params.minConvexity = 0.95
params.maxConvexity = 0.95
# Filter by Inertia
params.filterByInertia = True
params.minInertiaRatio = 0.01
params.maxInertiaRatio = 0.25
blobDetector = cv2.SimpleBlobDetector_create(params)

def print_coordinates(event, x, y, flags, param):
    if event == cv2.EVENT_LBUTTONDOWN:
        print(x, y)

while True:
    ret, frame = cap.read()
    if (not ret):
        print("Could not read")
        sys.exit(1)
    # keypoints = blobDetector.detect(image)
    # image_with_keypoints = cv2.drawKeypoints(image, keypoints, np.array([]), (0,0,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

    cv2.setMouseCallback("frame", print_coordinates)

    cv2.imshow("frame", frame)
    keyPress = cv2.waitKey(30) & 0xFF
    if (keyPress == ord('q')):
        break
    if (keyPress == ord('p')):
        cv2.imwrite("test.jpg", frame)

cap.release()
cv2.destroyAllWindows()
