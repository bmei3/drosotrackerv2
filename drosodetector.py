import cv2
import numpy as np

class Detector:
    def __init__(self):
        self.detector = None

    def init(self):
        # Setup SimpleBlobDetector parameters.
        params = cv2.SimpleBlobDetector_Params()

        params.filterByColor = False
        params.blobColor = 255

        # Change thresholds
        params.minThreshold = 1
        params.maxThreshold = 200

        # Filter by Area.
        params.filterByArea = True
        # params.minArea = 20
        params.minArea = 30
        params.maxArea = 180

        # Filter by Circularity
        params.filterByCircularity = False
        params.minCircularity = 0.1
        params.maxCircularity = 0.5

        # Filter by Convexity
        params.filterByConvexity = False
        params.minConvexity = 0.50
        params.maxConvexity = 0.95

        # Filter by Inertia
        params.filterByInertia = True
        params.minInertiaRatio = 0.15
        params.maxInertiaRatio = 0.25

        self.detector = cv2.SimpleBlobDetector_create(params)

        return self

    def detect(self, image):
        keypoints = self.detector.detect(image)
        image_with_keypoints = cv2.drawKeypoints(image, keypoints, np.array([]), (0,0,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
        return image_with_keypoints, keypoints